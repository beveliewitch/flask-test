from flask import Flask, request, jsonify, render_template
from uuid import uuid1

# this variable is only initialized once
my_prepared_var = "ok"
print(my_prepared_var)

app = Flask(__name__)


@app.route("/")
def index():
    return "It works !"


@app.route("/jsonify-example")
def jsonify_example():
    return jsonify({"name": "object", "type": "turtle", "token": uuid1()})


@app.route("/send-something", methods=['POST'])
def send_something():
    return "I receive this " + request.form['object']


@app.route("/render-template/", defaults={'sentence': None}, methods=['POST'])
@app.route("/render-template/<sentence>", methods=['GET'])
def render_my_template(sentence=None):
    sentence = request.form['sentence'] if 'sentence' in request.form else sentence
    return render_template("index.html", sentence=sentence)
