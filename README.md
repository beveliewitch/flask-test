# flask-test

A simple test project of micro-framework Flask for Python

# How to run

1. Install flask : `pip install Flask`
2. Run following commands in project directory
    1. `SET_FLASKAPP=main.py`
    2. `SET_ENV=Development`
    3. `flask run`
3. Open a web browser and go to [`http://127.0.0.1:5000`](http://127.0.0.1:5000)

# Routes

 - simple route [`http://127.0.0.1:5000`](http://127.0.0.1:5000)
 - jsonify example [`http://127.0.0.1:5000/jsonify-example`](http://127.0.0.1:5000/jsonify-example)
 - receive something by POST form [`http://127.0.0.1:5000/send-something`](http://127.0.0.1:5000/send-something)
 - render template by GET with parameter [`http://127.0.0.1:5000/render-template/`](http://127.0.0.1:5000/render-template/)
 - render template by POST with [`http://127.0.0.1:5000/render-template/<sentence>`](http://127.0.0.1:5000/render-template/test)

